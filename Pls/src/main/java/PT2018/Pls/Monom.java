package PT2018.Pls;



import java.util.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Comparator;
import java.util.regex.Pattern;
public class Monom {
	private String sign;
	private double coef;
	private int power;

	Monom(String s) {
		Pattern pattern= Pattern.compile("x\\^");
		String[] parts = pattern.split(s);
		//basic split into coeff and power
		this.coef = Double.parseDouble(parts[0]);
		this.power = Integer.parseInt(parts[1]);

	}

	Monom(double a, int b) {

		this.coef = a;
		this.power = b;
	}

	Monom() {
		this.coef = 0;
		this.power = 0;
	}

public static Comparator<Monom> Powercomp=new Comparator<Monom>(){
	public int compare(Monom m1,Monom m2){
		return m2.getpower()-m1.getpower();
		}
		
	};

	public String getsign() {
		return sign;
	}

	public void setcoef(double coef) {
		this.coef = coef;
	}

	public void setpower(int power) {
		this.power = power;
	}

	public double getcoef() {
		return this.coef;
	}

	public int getpower() {
		return power;
	}

	public Monom add(Monom monom) {//addition
		Monom a1 = new Monom(this.coef + monom.coef, this.power);

		return a1;
	}

	public Monom mul(Monom monom) {//multiplication
		Monom a2 = new Monom();
		a2.setcoef(this.coef * monom.coef);
		a2.setpower(this.power + monom.power);
		return a2;
	}

	public Monom sub(Monom monom) {//substraction
		Monom a1 = new Monom(this.coef - monom.coef, this.power);

		return a1;
	}

	public Monom div(Monom monom) {//division
		Monom a2 = new Monom();
		a2.setcoef(this.coef / monom.coef);
		a2.setpower(this.power - monom.power);
		return a2;
	}

}

