package PT2018.Pls;





public class PoliModel {
	//
	private Polinom polinom1;
	private Polinom polinom2;
	private Polinom result;

	PoliModel() {
		reset();
	}

	public void reset() {
		polinom1 = null;
		polinom2 = null;
		result = null;
	}

	public void addPoli() {
		result = polinom1.add(polinom2);
	}

	public void subPoli() {
		result = polinom1.sub(polinom2);
	}

	public void mulPoli() {
		result = polinom1.mul(polinom2);
	}

	public void divPoli() {
		result = polinom1.div(polinom2);
	}
	public void derPoli() {
		result = polinom1.der();
	}
	public void intPoli() {
		result = polinom1.int1();
	}

	public void setPoli12(String value1, String value2) {
		polinom1 = new Polinom(value1);
		polinom2 = new Polinom(value2);
	}


	public Polinom getResult() {
		return result;
	}
}

