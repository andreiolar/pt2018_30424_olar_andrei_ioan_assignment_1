package PT2018.Pls;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

public class PoliView extends JFrame implements ActionListener{
	//creates stuff
	private static final String INITIAL_VALUE = "1";
	private JButton addButton=new JButton();
	private JButton subButton;
	private JButton mulButton;
	private JButton divButton;
	private JButton derButton;
	private JButton intButton;
	private JLabel poliLabel1;
	private JLabel poliLabel2;
	private JLabel poliRezLabel;
	private JTextField poliText1;
	private JTextField poliText2;
	private JTextField poliRezText;
	private PoliModel model;

	PoliView(PoliModel model) {

		

		
		
		// Create and set up the content pane.
		JPanel content = new JPanel();
		
		content.setOpaque(true); // content panes must be opaque
		
		// Display the window.
		
		this.model = model;
		//gives values for created stuff
		poliLabel1 = new JLabel("Polinomul 1: ");
		poliLabel1.setBounds(200, 200,200, 200);
		content.add(poliLabel1);
		poliText1 = new JTextField(20);
		content.add(poliText1);

		poliLabel2 = new JLabel("Polinomul 2: ");
		poliLabel2.setBounds(20, 20, 20, 20);
		content.add(poliLabel2);
		poliText2 = new JTextField(20);
		content.add(poliText2);

		poliRezLabel = new JLabel("Rezultat : ");
		poliRezLabel.setVerticalTextPosition(JLabel.BOTTOM);
		poliRezLabel.setHorizontalTextPosition(JLabel.CENTER);
		poliRezLabel.setBounds(20, 20, 20, 20);
		content.add(poliRezLabel);

		poliRezText = new JTextField(20);
		content.add(poliRezText);

		addButton = new JButton("+");
		content.add(addButton);
		

		addButton.setActionCommand("1");

		subButton = new JButton("-");
		content.add(subButton);

		subButton.setActionCommand("2");

		mulButton = new JButton("*");
		content.add(mulButton);

		mulButton.setActionCommand("3");

		divButton = new JButton(":");
		content.add(divButton);

		divButton.setActionCommand("4");
		derButton=new JButton("derivate");
		content.add(derButton);
		derButton.setActionCommand("5");
		intButton=new JButton("integrate");
		content.add(intButton);
		intButton.setActionCommand("6");
		//add action listener
		addButton.addActionListener(this);
		subButton.addActionListener(this);
		mulButton.addActionListener(this);
		divButton.addActionListener(this);
		intButton.addActionListener(this);
		derButton.addActionListener(this);
		this.setContentPane(content);
		this.pack();
		this.setTitle("Operatii pe polinoame");
	}

	
	
		
	
		public void actionPerformed(ActionEvent e) {
			
			
		
	
			if(poliText2.getText()==null)
				model.setPoli12(poliText1.getText(),"0x^0");
			else
		model.setPoli12(poliText1.getText(),poliText2.getText());
		int op=Integer.parseInt(e.getActionCommand());//transforms the strings given by action command to integer
		switch (op) {//chooses right operation
		case 1:model.addPoli();poliRezText.setText(model.getResult().getpolinom());break;
		case 2:model.subPoli();poliRezText.setText(model.getResult().getpolinom());break;
		case 3:model.mulPoli();poliRezText.setText(model.getResult().getpolinom());break;
		case 4:model.divPoli();poliRezText.setText(model.getResult().getpolinom());break;
		case 5:model.derPoli();poliRezText.setText(model.getResult().getpolinom());break;
		case 6:model.intPoli();poliRezText.setText(model.getResult().getpolinom());break;
		default:;
		}
		
		
	
		
	
	

}}
