package PT2018.Pls;



import java.util.*;

public class Polinom {
	ArrayList<Monom> monoame = new ArrayList<Monom>();

	Polinom(String s) {

		String[] parts = s.split("\\+");//s se imparte intr-un vector de String-uri
		List<String> sublist1 = Arrays.asList(parts);//transformam vetor in lista
		if (sublist1.get(0).indexOf('-') == -1)//folosim sublist si verificam daca primul numar e <0
			monoame.add(new Monom(sublist1.get(0)));//daca nu facem monom
		else {
			String[] whyareyoulikethis = sublist1.get(0).split("-");//daca e <0 il impartim iar in string uri si vedem pasam tot la contructorul de monoame
			if (whyareyoulikethis[0].isEmpty()) {//daca primul string primit  e gol sarim peste primul element
				monoame.add(new Monom("-" + whyareyoulikethis[1]));
				List<String> sublist2 = Arrays.asList(whyareyoulikethis);
				for (String smallerpart : sublist2.subList(2, sublist2.size())) {
					monoame.add(new Monom("-" + smallerpart));
				}
			} else {
				monoame.add(new Monom(whyareyoulikethis[0]));//string nu e gold deci nu sarim peste primul element
				List<String> sublist2 = Arrays.asList(whyareyoulikethis);
				for (String smallerpart : sublist2.subList(1, sublist2.size())) {
					monoame.add(new Monom("-" + smallerpart));
				}
			}

		}
		for (String part : sublist1.subList(1, sublist1.size())) {//prima parte este pentru cazuri speciale
			if (part.indexOf('-') == -1) {//aici noi continuam citirea monoamelor pentru restul stringurilor

				monoame.add(new Monom(part));
			} else {
				String[] smallerParts = part.split("-");
				monoame.add(new Monom(smallerParts[0]));
				List<String> sublist = Arrays.asList(smallerParts);
				for (String smallerpart : sublist.subList(1, sublist.size())) {
					monoame.add(new Monom('-' + smallerpart));
				}
			}
		}
	}

	Polinom() {
	}

	public Polinom add(Polinom polinom) {
		Polinom pol = new Polinom();
		Collections.sort(this.monoame, Monom.Powercomp);//sort this poli
		Collections.sort(polinom.monoame, Monom.Powercomp);//sort polinom
		//add the coef of the same power from both poli and put them in the resulting polinomial
		for (Monom m : this.monoame)
			for (Monom m1 : polinom.monoame) {
				if (m.getpower() == m1.getpower())
					pol.monoame.add(m.add(m1));

			}
		for (Monom m : this.monoame) {
			int k = 0;
			for (Monom m1 : pol.monoame) {//add remaining elem from this
				if (m.getpower() == m1.getpower())
					k++;
			}
			if (k == 0)
				pol.monoame.add(m);
		}
		for (Monom m : polinom.monoame) {
			int k = 0;
			for (Monom m1 : pol.monoame) {//add remainin elem from polinom
				if (m.getpower() == m1.getpower())
					k++;
			}
			if (k == 0)
				pol.monoame.add(m);
		}
		for (Iterator<Monom> it = pol.monoame.iterator(); it.hasNext();) {//remove monoms with coeff 0

			Monom m = it.next();
			if (m.getcoef() == 0)
				it.remove();
		}
		Collections.sort(pol.monoame, Monom.Powercomp);//sort result
		return pol;
	}

	public Polinom sub(Polinom polinom) {
		Polinom pol = new Polinom();
		Collections.sort(this.monoame, Monom.Powercomp);
		Collections.sort(polinom.monoame, Monom.Powercomp);
		for (Monom m : this.monoame)//sub monoms with same power
			for (Monom m1 : polinom.monoame) {
				if (m.getpower() == m1.getpower())
					pol.monoame.add(m.sub(m1));

			}
		for (Monom m : this.monoame) {//add remaining elem from this
			int k = 0;
			for (Monom m1 : pol.monoame) {
				if (m.getpower() == m1.getpower())
					k++;
			}
			if (k == 0)
				pol.monoame.add(m);
		}
		for (Monom m : polinom.monoame) {//insert remaining elem from
			int k = 0;
			for (Monom m1 : pol.monoame) {
				if (m.getpower() == m1.getpower())
					k++;
			}
			if (k == 0) {
				Monom a = new Monom(0, m.getpower());
				pol.monoame.add(a.sub(m));
			}
		}
		for (Iterator<Monom> it = pol.monoame.iterator(); it.hasNext();) {//remove elem with 0 coeff

			Monom m = it.next();
			if (m.getcoef() == 0)
				it.remove();
		}
		Collections.sort(pol.monoame, Monom.Powercomp);//sort resul 
		return pol;
	}

	public Polinom mul(Polinom polinom) {
		Polinom pol = new Polinom();
		int k;
		for (Monom monom : this.monoame) {
			for (Monom monom1 : polinom.monoame) {
				k = 0;
				for (Monom m : pol.monoame) {//check if monom product has a similar power to any other monom inside pol
					if (m.getpower() == monom.mul(monom1).getpower()) {
						m.setcoef(m.add(monom.mul(monom1)).getcoef());//if yes sum

						k = 1;

						break;
					}
				}
				if (k == 0) {
					pol.monoame.add(monom.mul(monom1));//else add new monom
				}
			}
		}

		for (Iterator<Monom> it = pol.monoame.iterator(); it.hasNext();) {//remove elem with 0 coeff

			Monom m = it.next();
			if (m.getcoef() == 0)
				it.remove();
		}
		Collections.sort(pol.monoame, Monom.Powercomp);
		return pol;
	}

	public Polinom mul(Monom m1) {
		Polinom pol = new Polinom();

		for (Monom monom : this.monoame) {
			pol.monoame.add(monom.mul(m1));//multiply each monom with a polynom
		}
		for (Iterator<Monom> it = pol.monoame.iterator(); it.hasNext();) {
//if has coeff =0 remove
			Monom m = it.next();
			if (m.getcoef() == 0)
				it.remove();
		}
		Collections.sort(pol.monoame, Monom.Powercomp);
		return pol;
	}

	public Polinom div(Polinom polinom) {
		Polinom r;

		Polinom q = new Polinom();

		Collections.sort(this.monoame, Monom.Powercomp);
		Collections.sort(polinom.monoame, Monom.Powercomp);
		if ((this.monoame.get(0).getpower() < polinom.monoame.get(0).getpower()) || polinom.monoame.isEmpty())
			System.out.println("Division Error Bad input!");//small input check
		q.monoame.add(this.monoame.get(0).div(polinom.monoame.get(0)));//q gets first monom

		Collections.sort(q.monoame, Monom.Powercomp);

		r = polinom.mul(q.monoame.get(0));

		r = this.sub(r);//r gets rest

		while (!r.monoame.isEmpty() && r.monoame.get(0).getpower() >= polinom.monoame.get(0).getpower()) {// while r not empty and r max power > polinom max power
			q.monoame.add(r.monoame.get(0).div(polinom.monoame.get(0)));//add new monom to q
			Collections.sort(q.monoame, Monom.Powercomp);
			r = r.sub(polinom.mul(r.monoame.get(0).div(polinom.monoame.get(0))));// change r

		}

		return q.add(r);
	}

	public Polinom der() {
		Polinom pol = new Polinom();
		Collections.sort(this.monoame, Monom.Powercomp);
//derivative
		for (Monom m : this.monoame) {
			if(m.getpower()==0)
				break;
			else
			pol.monoame.add(new Monom(m.getpower()*m.getcoef(),m.getpower()-1));
			
		}
		Collections.sort(pol.monoame, Monom.Powercomp);
		return pol;

	}
	public Polinom int1() {
		Polinom pol = new Polinom();
		Collections.sort(this.monoame, Monom.Powercomp);
//integral
		for (Monom m : this.monoame) {
			
			pol.monoame.add(new Monom(m.getcoef()/(m.getpower()+1),m.getpower()+1));
			
		}
		Collections.sort(pol.monoame, Monom.Powercomp);
		return pol;

	}

	public String getpolinom() {
		if (monoame.isEmpty())
			return "0";
		else {
			Collections.sort(this.monoame, Monom.Powercomp);
			String s = "" + monoame.get(0).getcoef() + "X^" + monoame.get(0).getpower();//gets first monom and => coeffx^power
			for (Monom m : this.monoame.subList(1, this.monoame.size())) {
				if (m.getcoef() < 0)
					s = s + m.getcoef() + "X^" + m.getpower();//next negative monoms gets inserted directly
				else
					s = s + "+" + m.getcoef() + "X^" + m.getpower();//positive ones get a + in front
			}
			return s;
		}
	}
}
